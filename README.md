# SteelEye SRE Assignment

The goal of this assignment is to deploy a simple NGINX application to a Kubernetes cluster.

The application was deployed in a Minikube cluster using Terraform and Helm.

All development and testing was done on a local MacOS environment.

## How to Run

### Prerequisites

- [Terraform CLI](https://developer.hashicorp.com/terraform/downloads)
- [Minikube CLI](https://minikube.sigs.k8s.io/docs/start)

### Execution

```bash
## Change to the terraform directory
cd terraform

# Initialize Terraform
terraform init

# Create the infrastructure and deploy the application
terraform apply

# Check the app service's url
minikube service my-app-nginx -n steeleye-sre-assignment
```

## Author

- **Vasco Ramos:** [vascoalramos](https://github.com/vascoalramos)
