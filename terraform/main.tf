resource "minikube_cluster" "local_cluster" {
  vm           = local.minikube.vm
  driver       = local.minikube.driver
  cluster_name = local.minikube.cluster_name
  addons       = local.minikube.addons
}

resource "helm_release" "app_release" {
  name             = "my-app"
  chart            = "${path.module}/../helm"
  namespace        = "steeleye-sre-assignment"
  create_namespace = true
  wait             = false
}
