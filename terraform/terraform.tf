terraform {
  required_version = "1.4.4"

  required_providers {
    minikube = {
      source  = "scott-the-programmer/minikube"
      version = "0.2.4"
    }

    helm = {
      source  = "hashicorp/helm"
      version = "2.9.0"
    }
  }
}

provider "minikube" {
  kubernetes_version = "v1.26.3"
}

provider "helm" {
  kubernetes {
    host = minikube_cluster.local_cluster.host

    client_certificate     = minikube_cluster.local_cluster.client_certificate
    client_key             = minikube_cluster.local_cluster.client_key
    cluster_ca_certificate = minikube_cluster.local_cluster.cluster_ca_certificate
  }
}
