locals {
  minikube = {
    vm           = true
    driver       = "hyperkit"
    cluster_name = "minikube"
    addons = [
      "dashboard",
      "default-storageclass",
      "metrics-server",
      "storage-provisioner",
    ]
  }

  manifests_src_path = "${path.module}/src/manifests"
}
